<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 8:58 PM
 */

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AbstractApiContext
{

    /** @var Curl */
    protected $curl;

    /** @var ContainerInterface */
    protected $container;

    /** @var string */
    protected $apiUrl;

    /** @var string */
    protected $token;

    const METHOD_GET = 1;
    const METHOD_POST = 2;
    const METHOD_HEAD = 3;
    const METHOD_PUT = 4;
    const METHOD_PATCH = 5;
    const METHOD_DELETE = 6;




    /**
     * ApiContext constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->apiUrl = $this->getParameter('api')['central']['url'];
        $this->token = $this->getParameter('api')['central']['token'];

        $this->curl = new Curl();
        $this->curl->setJsonDecoder(function () {
            $args = func_get_args();

            $response = json_decode($args[0], true);
            if ($response === null) {
                $response = $args[0];
            }
            return $response;
        });
    }

    protected function generateApiUrl(string $rawEndpoint, array $params)
    {
        $result = $rawEndpoint;
        foreach ($params as $paramName => $paramValue) {
            $result = str_replace('{'.$paramName.'}', urlencode($paramValue), $result);
        }

        return $result;
    }

    /**
     * @param string $endpoint
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    protected function makeQuery(string $endpoint, string $httpMethod, array $data = [])
    {
        $url = $this->apiUrl . $endpoint;
        $data = array_merge(['token' => $this->token], $data);

        switch ($httpMethod) {
            case self::METHOD_GET:
                $this->curl->get($url, $data);
                break;
            case self::METHOD_PUT:
                $this->curl->put($url, $data);
                break;
            case self::METHOD_POST:
                $this->curl->post($url, $data);
                break;
            case self::METHOD_PATCH:
                $this->curl->patch($url, $data);
                break;
            case self::METHOD_HEAD:
                $this->curl->head($url, $data);
                break;
            case self::METHOD_DELETE:
                $this->curl->delete($url, $data);
                break;
            default:
                break;
        }

        if ($this->curl->error) {
            if($httpMethod == self::METHOD_HEAD && $this->curl->error == 404) {
                return false;
            }
            throw new ApiException(
                $this->curl->errorMessage,
                $this->curl->errorCode,
                null,
                $this->curl->response
            );
        } else {
            if($httpMethod == self::METHOD_HEAD) {
                return true;
            }
            return $this->curl->response;
        }
    }

    protected function getParameter(string $name)
    {
        return $this->container->getParameter($name);
    }
}
